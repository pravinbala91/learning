from devices import IotCore
from model import Sensor, Sprinkler
from publisher import Publish

sensors = []
sprinklers = []

iot_ref = IotCore()


# Get Sensors and Sprinklers
def get_sensors_and_sprinklers():
    groups = iot_ref.iot.list_thing_groups()
    for group in groups['thingGroups']:
        thing_group_desc = iot_ref.iot.describe_thing_group(
            thingGroupName=group['groupName']
        )
        things = iot_ref.iot.list_things_in_thing_group(
            thingGroupName=group['groupName']
        )
        for thing in things['things']:
            attributes = thing_group_desc['thingGroupProperties']['attributePayload']['attributes']
            if thing.startswith('sensor'):
                sensors.append(Sensor(thing, attributes['topic'], attributes['loc_lat'], attributes['loc_long'],
                                      group['groupName']))
            elif thing.startswith('sprinkler'):
                sprinklers.append(Sprinkler(thing, attributes['topic'], attributes['loc_lat'], attributes['loc_long'],
                                            group['groupName']))


if __name__ == '__main__':
    print("Agri Tech Project")
    print("-----------------")
    while True:
        print("Select the operation mode")
        print("*************************")
        print("1. Generate AWS stuffs \t 2. Do Irrigation \t 3. Exit")
        choice = int(input("Enter the option : "))
        if choice == 1:
            policyName = "AgriTech_Policy"
            iot_ref.create_AgriTech(policyName)
        elif choice == 2:
            get_sensors_and_sprinklers()
            for sensor in sensors:
                sen = Publish(sensor)
                sen.publish()
        elif choice == 3:
            print("exit")
            break
        else:
            print("wrong option")
