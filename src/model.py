
class AWSIotThing:

    def __init__(self, name, topic, latitude, longitude, thing_group):
        self._name = name
        self._topic = topic
        self._latitude = latitude
        self._longitude = longitude
        self._thing_group = thing_group

    @property
    def name(self):
        return self._name

    @property
    def topic(self):
        return self._topic

    @property
    def latitude(self):
        return self._latitude

    @property
    def longitude(self):
        return self._longitude

    @property
    def thing_group(self):
        return self._thing_group

    def __str__(self):
        return f'name - ', {self.name},' topic - ', {self.topic}, ' latitude - ', {self.latitude}, ' longtidue - ', {self.longitude}, ' thing_group - ', {self.thing_group}


class Sensor(AWSIotThing):
    def __init__(self, name, topic, latitude, longitude, thing_group):
        super().__init__(name, topic, latitude, longitude, thing_group)


class Sprinkler(AWSIotThing):
    def __init__(self, name, topic, latitude, longitude, thing_group):
        super().__init__(name, topic, latitude, longitude, thing_group)