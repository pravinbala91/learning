import boto3


class Credentials:

    def __init__(self):
        pass

    # Get Access using the credentials file
    def get_credential_access(self):
        aws_credential = '../res/credentials.txt'
        access = {}
        with open(aws_credential, 'r') as aws_file:
            cred_lines = [line.rstrip() for line in aws_file]

        for cred in cred_lines:
            if cred.startswith('aws_session_token'):
                access['aws_session_token'] = cred[len('aws_session_token') + 1:]
            else:
                key, value = cred.split("=")
                access[key] = value

        return access

    # Creating a session for boto3 client
    def create_boto3_client_session(self, resource):
        access = self.get_credential_access()
        return boto3.client(
            resource,
            aws_access_key_id=access['aws_access_key_id'],
            aws_secret_access_key=access['aws_secret_access_key'],
            aws_session_token=access['aws_session_token'])