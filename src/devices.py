# User Define
from access import Credentials
from constants import PATH_TO_CERT

# Pre Define
import os
import json
from botocore.exceptions import ClientError


class IotCore:

    def __init__(self):
        setup_json = open("../res/config/setup.json")
        self.setup = json.load(setup_json)
        self.iot = Credentials().create_boto3_client_session('iot')

    # Create a sensor thing
    def create_thing_type(self):
        try:
            thing_types = self.setup['types']
            for thing_type in thing_types:
                thingType = self.iot.create_thing_type(
                    thingTypeName=thing_type['name'],
                    thingTypeProperties={
                        'thingTypeDescription': thing_type['description']
                    }
                )
                print(thingType)
            print("==================================== Type Created ====================================")
        except ClientError as e:
            print(e)

    # Create a thing
    def create_thing(self):
        try:
            iot_things = self.setup['things']
            for iot_thing in iot_things:
                thing = self.iot.create_thing(
                    thingName=iot_thing['name'],
                    thingTypeName=iot_thing['type'],
                    attributePayload={
                        "attributes": {
                            "thing_group": iot_thing['thing_group']
                        }
                    }
                )
                print(thing)
            print("==================================== Things Created ====================================")
        except ClientError as e:
            print(e)

    # Create a sensor group
    def create_thing_group(self):
        try:
            thing_groups = self.setup['things_group']
            for thing_group in thing_groups:
                group = self.iot.create_thing_group(
                    thingGroupName=thing_group['name'],
                    thingGroupProperties={
                        'thingGroupDescription': thing_group['desc'],
                        'attributePayload': {
                            'attributes': {
                                'topic': thing_group['topic'],
                                'loc_lat': str(thing_group['location']['lat']),
                                'loc_long': str(thing_group['location']['long'])
                            }
                        }
                    }
                )
                print(group)
            print("==================================== Groups Created ====================================")
        except ClientError as e:
            print(e)

    # Create a policy
    def create_policy(self, name):
        try:
            docu = {
                "Version": "2012-10-17",
                "Statement": [{
                    "Effect": "Allow",
                    "Action": [
                        "iot:*"
                    ],
                    "Resource": ["*"]
                }]
            }

            policy = self.iot.create_policy(
                policyName=name,
                policyDocument=json.dumps(docu)
            )
            print(policy)
            print("==================================== Policy Created ====================================")
        except ClientError as e:
            print(e)

    # Policy attachment to thing group
    def attach_policy(self, name):
        try:
            groups = self.iot.list_thing_groups()
            for group in groups['thingGroups']:
                attach = self.iot.attach_policy(
                    policyName=name,
                    target=group['groupArn']
                )
                print(attach)
            print("==================================== Policy Attached ====================================")
        except ClientError as e:
            print(e)

    # Groping things
    def group_things(self):
        try:
            things_list = self.iot.list_things()
            groups_list = self.iot.list_thing_groups()
            for things in things_list['things']:
                for groups in groups_list['thingGroups']:
                    if things['attributes']['thing_group'] == groups['groupName']:
                        grouping = self.iot.add_thing_to_thing_group(
                            thingGroupName=groups['groupName'],
                            thingGroupArn=groups['groupArn'],
                            thingName=things['thingName'],
                            thingArn=things['thingArn']
                        )
                        print(grouping)
            print("==================================== Things are Grouped ====================================")
        except ClientError as e:
            print(e)

    # Sensor Class
    def create_certificate_with_keys(self):
        try:
            certificate = self.iot.create_keys_and_certificate(
                setAsActive=True
            )
            file_name = certificate['certificateId'][:10]
            file_path = "../res/certificate/" + file_name
            self.store_generate_pem_file(file_path, "-certificate.pem.crt", "w+", certificate['certificatePem'])
            self.store_generate_pem_file(file_path, "-public.pem.key", "w+", certificate['keyPair']['PublicKey'])
            self.store_generate_pem_file(file_path, "-private.pem.key", "w+", certificate['keyPair']['PrivateKey'])
            print("==================================== Certificate is generated ====================================")
        except ClientError as e:
            print(e)

    # General for pem file creation
    def store_generate_pem_file(self, file_path, file_type, operation, data):
        file = open(file_path + file_type, operation)
        split_data = data.split("\n")
        for split in split_data:
            file.writelines(split + "\n")
        file.close()
        print("==================================== Pem Files Generated ====================================")

    # Create the Agri Tech System in single shot
    def create_AgriTech(self, policyName):

        # 1. Create Types
        self.create_thing_type()

        # 2. Create Certificate and Policy
        self.create_certificate_with_keys()
        self.create_policy(policyName)

        # 3. Create Thing
        self.create_thing()

        # 4. Create Thing group
        self.create_thing_group()

        # 5. Group Things
        self.group_things()

        # 6. Attach Policy to Thing Group
        self.attach_policy(policyName)


    # Get the endpoint of Iot
    def get_endpoint(self):
        aws_endpoint = self.iot.describe_endpoint()
        return aws_endpoint['endpointAddress']

    # Get the files with location
    def get_pem_files(self):
        iot_pem = {}
        iot_certificate_pem = [cert for cert in os.listdir(PATH_TO_CERT)]
        for cert in iot_certificate_pem:
            if cert.endswith(".crt"):
                iot_pem['certificate_crt'] = cert
            elif cert.__contains__("private"):
                iot_pem['private'] = cert
            elif cert.__contains__("public"):
                iot_pem['public'] = cert
        return iot_pem