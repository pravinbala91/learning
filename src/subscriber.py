# User Define
from devices import IotCore
from constants import PATH_TO_CERT, PATH_TO_CA

from main import get_sensors_and_sprinklers, sprinklers

# Pre Define
import time
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

iot_ref = IotCore()
pem_and_keys = iot_ref.get_pem_files()

endpoint = "ayk1lu6mahlt6-ats.iot.us-east-1.amazonaws.com"

def callback(client, userdate, message):
    print(message.payload)

class Subscribe:

    def __init__(self, device):
        self.device = device
        self.cert_path = PATH_TO_CERT + pem_and_keys['certificate_crt']
        self.pvt_key_path = PATH_TO_CERT + pem_and_keys['private']
        self.root_path = PATH_TO_CA + 'AmazonRootCA1.pem'
        self.myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(device.name)
        self.myAWSIoTMQTTClient.configureEndpoint(endpoint, 8883)
        self.myAWSIoTMQTTClient.configureCredentials(self.root_path, self.pvt_key_path, self.cert_path)
        self._connect()

    def _connect(self):
        self.myAWSIoTMQTTClient.connect()

    def _disconnect(self):
        self.myAWSIoTMQTTClient.disconnect()

    def subscribe(self):
        print("Subscriber Started to Listen")
        self.myAWSIoTMQTTClient.subscribe("iot/topic#", 1, callback)

    def callback(self, client, userdate, message):
        print(client)
        print(userdate)
        print(message)

if __name__ == "__main__":
    get_sensors_and_sprinklers()
    while True:
        for sprinkler in sprinklers:
            sub = Subscribe(sprinkler)
        try:
            sub.subscribe()
        except KeyboardInterrupt:
            sub.myAWSIoTMQTTClient.disconnect()
