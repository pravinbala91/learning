# User Define
from devices import IotCore
from constants import PATH_TO_CERT, PATH_TO_CA

# Pre Define
import json
import time
import datetime
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

from random import randint
from socket import timeout
from pyowm import OWM

# Constants
iot_ref = IotCore()
pem_and_keys = iot_ref.get_pem_files()
owm = OWM('852d42d3545c1560ffa4795aa5cd6e60')
mgr = owm.weather_manager()
endpoint = "ayk1lu6mahlt6-ats.iot.us-east-1.amazonaws.com"


class Publish:

    def __init__(self, device):
        self.device = device
        self.iot = iot_ref.iot
        self.cert_path = PATH_TO_CERT + pem_and_keys['certificate_crt']
        self.pvt_key_path = PATH_TO_CERT + pem_and_keys['private']
        self.root_path = PATH_TO_CA + 'AmazonRootCA1.pem'
        self.myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(device.name)
        self.myAWSIoTMQTTClient.configureEndpoint(endpoint, 8883)
        self.myAWSIoTMQTTClient.configureCredentials(self.root_path, self.pvt_key_path, self.cert_path)
        self._connect()

    def _connect(self):
        self.myAWSIoTMQTTClient.connect()

    def _disconnect(self):
        self.myAWSIoTMQTTClient.disconnect()

    def weather_data(self):
        try:
            mgr = owm.weather_manager()
            one_call = mgr.one_call(lat=int(self.device.latitude), lon=int(self.device.longitude))
            current_weather_data = json.dumps(one_call.current.__dict__)
            current_weather_data = json.loads(current_weather_data)
            return current_weather_data
        except timeout as e:
            print('Weather timeout error')

    def publish(self):
        print("-------------------------------------")
        print("Start publishing : ", self.device.name)
        message = {}
        weather = self.weather_data()
        time.sleep(0.5)
        message['device_name'] = self.device.name
        message['timestamp'] = str(datetime.datetime.now())
        message['moisture'] = randint(50, 100)
        value = weather['temp']['temp'] - 273
        message['temperature'] = round(value, 1)
        message['climate'] = weather['status']
        message['humidity'] = weather['humidity']
        message['plot'] = self.device.thing_group
        message['topic'] = self.device.topic
        message['latitude'] = int(self.device.latitude)
        message['longitude'] = int(self.device.longitude)
        messageJson = json.dumps(message)
        self.myAWSIoTMQTTClient.publish(self.device.topic, messageJson, 1)
        print(self.device.topic, " -> ", messageJson)
        print("Stop publishing : ", self.device.name)
        print("-------------------------------------")
